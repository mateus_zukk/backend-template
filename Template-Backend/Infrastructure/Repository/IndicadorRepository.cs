﻿using Microsoft.EntityFrameworkCore;
using PNAD_Backend.Domain.Interfaces;
using PNAD_Backend.Interfaces;
using PNAD_Backend.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Linq.Expressions;
using PNAD_Backend.Domain.DTO;

namespace PNAD_Backend.Domain.Repository
{
    public class IndicadorRepository : IIndicadorRepository
    {
        private readonly PnadDBContext _context;

        public IndicadorRepository(PnadDBContext context)
        {
            this._context = context;
        }

        public IUnitOfWork UnitOfWork => _context;

        public async Task<Indicador> Add(Indicador element)
        {
            await _context.Indicadores.AddAsync(element);
            await _context.SaveChangesAsync();
            return element;
        }

        public async Task<Comparacao> AddComparacao(Comparacao comparacao)
        {
            await _context.Comparacoes.AddAsync(comparacao);
            await _context.SaveChangesAsync();
            return comparacao;
        }

        public async Task<bool> Change(Indicador element)
        {
            _context.Entry(element).State = EntityState.Modified;
            return (await _context.SaveChangesAsync() > 0);
        }

        public async Task<bool> Remove(Int32 guid)
        {
            var element = await _context.Indicadores.Include(p => p.Comparacoes).Where(p => p.Id == guid).FirstOrDefaultAsync();

            _context.RemoveRange(element.Comparacoes); 
            _context.Remove(element);

            return (await _context.SaveChangesAsync() > 0);
        }

        public async Task<bool> ComparacaoRemove(Comparacao comparacao)
        {
            _context.Comparacoes.Remove(comparacao);
            return (await _context.SaveChangesAsync() > 0);
        }

        public async Task<Indicador> GetFirstAllDefault()
        {
            return await _context.Indicadores.FirstOrDefaultAsync();
        }

        public async Task<Indicador> Find(int key)
        {
            return await _context.Indicadores.Include(p => p.Comparacoes).Where(p => p.Id == key).FirstOrDefaultAsync();
        }

        public async Task<IList<Indicador>> FindAll()
        {
            return await _context.Indicadores.Include(i => i.Tema).Include(i => i.Recorte).ToListAsync();
        }

        public async Task<IEnumerable<IndicadorResponseDTO>> FindIndicators()
        {
            return await (from ind in _context.Indicadores
                          join t in _context.Temas on ind.TemaId equals t.Id
                          join r in _context.Recortes on ind.RecorteGeograficoId equals r.Id
                          orderby t.Ordem, r.Ordem, ind.Ordem
                          select new IndicadorResponseDTO()
                          {
                              Id = ind.Id.ToString(),
                              Tema = t.Nome,
                              DesagregacaoRegional = r.Nome,
                              Indicadores = ind.Indicadores,
                              NomeCard = ind.Card,
                              FiltroEspacial = ind.FiltroEspacial,
                              Temporalidade = ind.Temporalidade,
                              NomeCampoTematico = ind.CampoTematico,
                              NomeCampoTemporal = ind.CampoTemporal,
                              NomeEntidadePoligonal = ind.CamadaWMS,
                              UrlPolgono = ind.UrlGeoServer,
                              SLDPoligono = ind.Sld,
                              NomeFeicaoGrafico = ind.CamadaWFS,
                              UrlPonto = ind.UrlGeoServer,
                              UrlGeoserver = ind.UrlGeoServer
                          }
                          ).ToListAsync();
        }

        public void Dispose()
        {
            _context.Dispose();
        }
    }
}
