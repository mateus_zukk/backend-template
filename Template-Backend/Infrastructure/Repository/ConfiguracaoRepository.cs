﻿using Microsoft.EntityFrameworkCore;
using PNAD_Backend.Domain.Interfaces;
using PNAD_Backend.Interfaces;
using PNAD_Backend.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Linq.Expressions;

namespace PNAD_Backend.Domain.Repository
{
    public class ConfiguracaoRepository : IConfiguracaoRepository
    {
        private readonly PnadDBContext _context;

        public ConfiguracaoRepository(PnadDBContext context)
        {
            this._context = context;
        }

        public IUnitOfWork UnitOfWork => _context;

        public async Task<Configuracao> Add(Configuracao element)
        {
            await _context.Configuracoes.AddAsync(element);
            await _context.SaveChangesAsync();
            return element;
        }

        public async Task<bool> Change(Configuracao element)
        {
            _context.Entry(element).State = EntityState.Modified;
            return (await _context.SaveChangesAsync() == 1);
        }

        public async Task<bool> Remove(Guid guid)
        {
            var element = await _context.Configuracoes.FindAsync(guid);
            _context.Configuracoes.Remove(element);
            return (await _context.SaveChangesAsync() == 1);
        }

        public async Task<Configuracao> GetFirstAllDefault()
        {
            return await _context.Configuracoes.FirstOrDefaultAsync();
        }

        public void Dispose()
        {
            _context.Dispose();
        }

        
    }
}
