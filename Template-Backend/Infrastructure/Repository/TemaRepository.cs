﻿using Microsoft.EntityFrameworkCore;
using PNAD_Backend.Domain.Interfaces;
using PNAD_Backend.Interfaces;
using PNAD_Backend.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Linq.Expressions;

namespace PNAD_Backend.Domain.Repository
{
    public class TemaRepository : ITemaRepository
    {
        private readonly PnadDBContext _context;

        public TemaRepository(PnadDBContext context)
        {
            this._context = context;
        }

        public IUnitOfWork UnitOfWork => _context;

        public async Task<Tema> Add(Tema element)
        {
            await _context.Temas.AddAsync(element);
            await _context.SaveChangesAsync();
            return element;
        }

        public async Task<bool> Change(Tema tema)
        {
            _context.Entry(tema).State = EntityState.Modified;
            return (await _context.SaveChangesAsync() == 1);
        }

        public async Task<bool> Remove(int guid)
        {
            var element = await _context.Temas.FindAsync(guid);
            _context.Temas.Remove(element);
            return (await _context.SaveChangesAsync() == 1);
        }

        public async Task<IList<Tema>> GetAll()
        {
            return await _context.Temas.OrderBy(p => p.Ordem).ToListAsync();
        }

        public void Dispose()
        {
            _context.Dispose();
        }

        
    }
}
