﻿using Microsoft.EntityFrameworkCore;
using PNAD_Backend.Domain.Interfaces;
using PNAD_Backend.Interfaces;
using PNAD_Backend.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Linq.Expressions;

namespace PNAD_Backend.Domain.Repository
{
    public class RecorteRepository : IRecorteRepository
    {
        private readonly PnadDBContext _context;

        public RecorteRepository(PnadDBContext context)
        {
            this._context = context;
        }

        public IUnitOfWork UnitOfWork => _context;

        public async Task<RecorteGeografico> Add(RecorteGeografico element)
        {
            await _context.Recortes.AddAsync(element);
            await _context.SaveChangesAsync();
            return element;
        }

        public async Task<bool> Change(RecorteGeografico tema)
        {
            _context.Entry(tema).State = EntityState.Modified;
            return (await _context.SaveChangesAsync() == 1);
        }

        public async Task<bool> Remove(int guid)
        {
            var element = await _context.Recortes.FindAsync(guid);
            _context.Recortes.Remove(element);
            return (await _context.SaveChangesAsync() == 1);
        }

        public async Task<IList<RecorteGeografico>> GetAll()
        {
            return await _context.Recortes.OrderBy(p => p.Ordem).ToListAsync();
        }

        public void Dispose()
        {
            _context.Dispose();
        }

        
    }
}
