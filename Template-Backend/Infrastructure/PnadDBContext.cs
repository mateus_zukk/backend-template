﻿using Microsoft.EntityFrameworkCore;
using PNAD_Backend.Interfaces;
using PNAD_Backend.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PNAD_Backend.Domain
{
    public class PnadDBContext : DbContext, IUnitOfWork
    {
        public DbSet<Tema> Temas { get; set; }
        public DbSet<RecorteGeografico> Recortes { get; set; }
        public DbSet<Configuracao> Configuracoes { get; set; }
        public DbSet<Indicador> Indicadores { get; set; }
        public DbSet<Comparacao> Comparacoes { get; set; }

        public PnadDBContext(DbContextOptions<PnadDBContext> options) : base(options)
        {
            ChangeTracker.QueryTrackingBehavior = QueryTrackingBehavior.NoTracking;
            ChangeTracker.AutoDetectChangesEnabled = false;
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            var entityTypes = modelBuilder.Model.GetEntityTypes();

            foreach (var property in entityTypes.SelectMany(
                e => e.GetProperties().Where(p => p.ClrType == typeof(string))))
                property.SetColumnType("varchar(255)");

            foreach (var relationship in entityTypes
                .SelectMany(e => e.GetForeignKeys())) relationship.DeleteBehavior = DeleteBehavior.ClientSetNull;

            modelBuilder.ApplyConfigurationsFromAssembly(typeof(PnadDBContext).Assembly);
            modelBuilder.HasDefaultSchema("pnadc");
            base.OnModelCreating(modelBuilder);
        }

        public async Task<bool> Commit()
        {
            var success = await base.SaveChangesAsync() > 0;
            return success;
        }
    }
}
