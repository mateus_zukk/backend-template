﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using PNAD_Backend.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PNAD_Backend.Domain.Mapping
{
    public class IndicadorMapping : IEntityTypeConfiguration<Indicador>
    {
        public void Configure(EntityTypeBuilder<Indicador> builder)
        {
            //builder.Property(p => p.UrlPoligono)
            //    .HasColumnType("text");            

            builder.HasOne(c => c.Tema);
            builder.HasOne(c => c.Recorte);
            builder.HasMany(c => c.Comparacoes);
        }
    }
}
