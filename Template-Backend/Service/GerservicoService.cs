﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PNAD_Backend.Service
{
    public class GeoservicoService
    {
        public string Url { get; set; }
        public string Authorization { get; set; }
        public string Workspace { get; set; }
    }
}
