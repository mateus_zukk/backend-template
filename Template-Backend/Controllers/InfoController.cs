﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PNAD_Backend.Controllers
{
    [ApiController]
    [Route("info")]
    public class InfoController : ControllerBase
    {
        public InfoController()
        {

        }

        [HttpGet("dateinfo")]
        public async Task<IActionResult> DateInfo()
        {
            var dateNow = DateTime.Now.ToString("dd/MM/yyyy");
            return Ok(new { date = dateNow });
        }
    }
}
