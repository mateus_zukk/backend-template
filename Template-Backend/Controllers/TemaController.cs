﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using PNAD_Backend.Domain.Interfaces;
using PNAD_Backend.Models;

namespace PNAD_Backend.Controllers
{
    [Route("api/[controller]")]
    [Authorize]
    public class TemaController : MainController
    {
        private readonly ITemaRepository repository;

        public TemaController(ITemaRepository repository)
        {
            this.repository = repository;
        }

        [HttpGet("")]
        public async Task<ActionResult> Get()
        {
            try
            {
                var response = await repository.GetAll();
                return CustomResponse(response);
            }
            catch (Exception ex)
            {
                AdicionarErroProcessamento(ex.Message);
                return CustomResponse();
            }
        }

        [HttpPost("Add")]
        public async Task<ActionResult> Add(Tema tema)
        {
            try
            {
                await repository.Add(tema);
                return CustomResponse(tema);
            }
            catch (Exception ex)
            {
                AdicionarErroProcessamento(ex.Message);
                return CustomResponse();
            }
        }

        [HttpPost("Change")]
        public async Task<ActionResult> Change(Tema tema)
        {
            try
            {
                var response = await repository.Change(tema);
                return CustomResponse(response);
            }
            catch (Exception ex)
            {
                AdicionarErroProcessamento(ex.Message);
                return CustomResponse();
            }
        }

        [HttpDelete("Delete/{id}")]
        public async Task<ActionResult> Delete(int Id)
        {
            try
            {
                var response = await repository.Remove(Id);
                return CustomResponse(response);
            }
            catch (Exception ex)
            {
                AdicionarErroProcessamento(ex.Message);
                return CustomResponse();
            }
        }
    }
}