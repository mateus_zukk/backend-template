﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using PNAD_Backend.Service;
using RestSharp;

namespace PNAD_Backend.Controllers
{
    [ApiController]
    [Route("api/geoservico")]
    public class GeoserviceController : ControllerBase
    {
        private readonly GeoservicoService gerservicoService;

        public GeoserviceController(IOptions<GeoservicoService> myConfiguration)
        {
            gerservicoService = myConfiguration.Value;
        }

        [HttpGet("api")]
        public async Task<IActionResult> Api()
        {
            try
            {
                if(Request.Query["service"].ToString().ToUpper() == "WMS")
                {
                    var url = $"{gerservicoService.Url}/{gerservicoService.Workspace}/wms{Request.QueryString.ToString()}";

                    var client = new RestClient(url);
                    client.Timeout = -1;
                    var request = new RestRequest(Method.GET);
                    request.AddHeader("Content-Type", "image/png");
                    request.AddHeader("Authorization", $"{gerservicoService.Authorization}");

                    //var response = await client.ExecuteAsync(request);
                    //return Ok(response.Content);

                    var fileBytes = client.DownloadData(request);

                    Response.Headers.Add("content-disposition", "inline;filename=" + "PNADC-uf_trimestral_poligono.png");
                    return File(fileBytes, "image/png");
                }
                else if (Request.Query["service"].ToString().ToUpper() == "WFS")
                {
                    var url = $"{gerservicoService.Url}/ows{Request.QueryString.ToString()}";

                    var client = new RestClient(url);
                    client.Timeout = -1;
                    var request = new RestRequest(Method.GET);
                    request.AddHeader("Content-Type", "application/json");
                    request.AddHeader("Authorization", $"{gerservicoService.Authorization}");

                    //var response = await client.ExecuteAsync(request);
                    //return Ok(response.Content);

                    IRestResponse response = await client.ExecuteAsync(request);                    
                    return Content(response.Content, "application/json");
                }
                else
                {
                    return Ok("parametros inválido");
                }
            }   
            catch (Exception ex)
            {
                return Ok(ex.Message);
            }
        }
        
        [HttpGet("legend/{style}")]
        public async Task<IActionResult> Legend(string style)
        {
            try
            {
                var url = $"{gerservicoService.Url}/wms?REQUEST=GetLegendGraphic&VERSION=1.0.0&FORMAT=image/png&WIDTH=20&HEIGHT=20&STRICT=false&style={style}";

                var client = new RestClient(url);
                client.Timeout = -1;
                var request = new RestRequest(Method.GET);
                request.AddHeader("Content-Type", "image/png");
                request.AddHeader("Authorization", $"{gerservicoService.Authorization}");

                var fileBytes = client.DownloadData(request);

                Response.Headers.Add("content-disposition", "inline;filename=" + $"PNADC-legend-{style}.png");
                return File(fileBytes, "image/png");
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError,
                    ex.Message);
            }
        }

        [HttpGet("wfs/fields")]
        public async Task<IActionResult> WfsFields(string url, string layer)
        {
            try
            {
                var _url = $"{url}/ows?service=WFS&version=1.0.0&request=GetFeature&typeName={layer}&maxFeatures=1&outputFormat=csv";

                var client = new RestClient(_url);
                client.Timeout = -1;
                var request = new RestRequest(Method.GET);
                request.AddHeader("Content-Type", "image/png");
                request.AddHeader("Authorization", $"{gerservicoService.Authorization}");

                IRestResponse response = await client.ExecuteAsync(request);

                if(response.Content.Contains("xml") || response.Content.Contains("doctype"))
                {
                    return StatusCode(StatusCodes.Status500InternalServerError,
                                        "Verifique se o serviço e a URL do Geoserviço informado está correto");
                }

                return Ok(response.Content.Split("\n")[0].Replace("\r", "").Split(','));
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError,
                    ex.Message);
            }
        }
    }
}