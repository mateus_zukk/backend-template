﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using PNAD_Backend.Domain.DTO;
using PNAD_Backend.Domain.Interfaces;
using PNAD_Backend.Models;

namespace PNAD_Backend.Controllers
{
    [Route("api/[controller]")]
    [Authorize]
    public class ConfiguracaoController : MainController
    {
        private readonly IConfiguracaoRepository repository;

        public ConfiguracaoController(IConfiguracaoRepository repository)
        {
            this.repository = repository;
        }

        [HttpGet("")]
        public async Task<ActionResult> Get()
        {
            try
            {
                var response = await repository.GetFirstAllDefault();
                return CustomResponse(response);
            }
            catch (Exception ex)
            {
                AdicionarErroProcessamento(ex.Message);
                return CustomResponse();
            }
        }

        [HttpPost("")]        
        public async Task<ActionResult> Add(ConfiguracaoDTO element)
        {
            try
            {
                var config = await repository.GetFirstAllDefault();

                if(config is null)
                {
                    config = new Configuracao();
                    config = DtoToEntity(config, element);
                    await repository.Add(config);                    
                }
                else
                {
                    config = DtoToEntity(config, element);
                    await repository.Change(config);
                }
                
                return CustomResponse(element);
            }
            catch (Exception ex)
            {
                AdicionarErroProcessamento(ex.Message);
                return CustomResponse();
            }
        }

        private Configuracao DtoToEntity(Configuracao config, ConfiguracaoDTO element)
        {
            config.DataAtualizacao = element.DataAtualizacao;
            config.Label1 = element.Label1;
            config.Label2 = element.Label2;
            config.Label3 = element.Label3;
            config.Label4 = element.Label4;
            config.Link = element.Link;
            config.Logo = element.Logo;
            config.Titulo = element.Titulo;

            return config;
        }

        [HttpDelete("Delete")]
        [Authorize]
        public async Task<ActionResult> Delete(Guid Id)
        {
            try
            {
                var response = await repository.Remove(Id);
                return CustomResponse(response);
            }
            catch (Exception ex)
            {
                AdicionarErroProcessamento(ex.Message);
                return CustomResponse();
            }
        }
    }
}