﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using PNAD_Backend.Domain.DTO;
using PNAD_Backend.Domain.Interfaces;
using PNAD_Backend.Models;

namespace PNAD_Backend.Controllers
{
    [Route("api/[controller]")]    
    public class IndicadorController : MainController
    {        
        private readonly IIndicadorRepository repository;

        public IndicadorController(IIndicadorRepository repository)
        {            
            this.repository = repository;
        }

        [HttpGet("")]
        [Authorize]
        public async Task<ActionResult> GetAll()
        {
            try
            {
                var response = await repository.FindAll();
                return CustomResponse(response);
            }
            catch (Exception ex)
            {
                AdicionarErroProcessamento(ex.Message);
                return CustomResponse();
            }
        }

        [HttpGet("indicadores")]
        public async Task<ActionResult> Indicadores()
        {
            try
            {
                var response = await repository.FindIndicators();
                return CustomResponse(response);
            }
            catch (Exception ex)
            {
                AdicionarErroProcessamento(ex.Message);
                return CustomResponse();
            }
        }

        [HttpGet("{Id}")]
        [Authorize]
        public async Task<ActionResult> Get(Int32 Id)
        {
            try
            {
                var response = await repository.Find(Id);
                return CustomResponse(response);
            }
            catch (Exception ex)
            {
                AdicionarErroProcessamento(ex.Message);
                return CustomResponse();
            }
        }

        [HttpPost("Add")]
        [Authorize]
        public async Task<ActionResult> Add(IndicadorDTO param)
        {
            try
            {
                if (!ModelState.IsValid) return CustomResponse(ModelState);

                var indicador = new Indicador();
                indicador.CampoTematico = param.CampoTematico;
                indicador.CampoTemporal = param.CampoTemporal;
                indicador.Card = param.Card;
                indicador.FiltroEspacial = param.FiltroEspacial;
                indicador.Indicadores = param.Indicadores;                
                indicador.RecorteGeograficoId = param.RecorteGeograficoId;
                indicador.Sld = param.Sld;
                indicador.TemaId = param.TemaId;
                indicador.Temporalidade = param.Temporalidade;
                indicador.UrlGeoServer = param.UrlGeoServer;
                indicador.CamadaWFS = param.CamadaWFS;
                indicador.CamadaWMS = param.CamadaWMS;
                indicador.Icone = param.Icone;
                indicador.Ordem = param.Ordem;
                indicador.TipoGrafico = param.TipoGrafico;
                indicador.ValorMaximo = param.ValorMaximo;
                indicador.ValorMinimo = param.ValorMinimo;
                indicador.NomeUnidade = param.NomeUnidade;
                indicador.Descricao = param.Descricao;

                indicador.Comparacoes = new List<Comparacao>();

                foreach (var comparacao in param.Comparacoes)
                {
                    indicador.Comparacoes.Add(new Comparacao()
                    {
                        CampoTematico = comparacao.CampoTematico,
                        NomeIndicador = comparacao.NomeIndicador,
                        Sld = comparacao.Sld
                    });
                }

                await repository.Add(indicador);
                return CustomResponse(indicador);
            }
            catch (Exception ex)
            {
                AdicionarErroProcessamento(ex.Message);
                return CustomResponse();
            }
        }

        [HttpPost("Change")]
        [Authorize]
        public async Task<ActionResult> Change(IndicadorChangeDTO param)
        {
            try
            {
                if (!ModelState.IsValid) return CustomResponse(ModelState);

                var indicador = await repository.Find(param.Id);
                indicador.CampoTematico = param.CampoTematico;
                indicador.CampoTemporal = param.CampoTemporal;
                indicador.Card = param.Card;
                indicador.FiltroEspacial = param.FiltroEspacial;
                indicador.Indicadores = param.Indicadores;                
                indicador.RecorteGeograficoId = param.RecorteGeograficoId;
                indicador.Sld = param.Sld;
                indicador.TemaId = param.TemaId;
                indicador.Temporalidade = param.Temporalidade;
                indicador.UrlGeoServer = param.UrlGeoServer;
                //indicador.UrlPoligono = param.UrlPoligono;
                indicador.CamadaWFS = param.CamadaWFS;
                indicador.CamadaWMS = param.CamadaWMS;
                indicador.Icone = param.Icone;
                indicador.Ordem = param.Ordem;
                indicador.TipoGrafico = param.TipoGrafico;
                indicador.ValorMaximo = param.ValorMaximo;
                indicador.ValorMinimo = param.ValorMinimo;
                indicador.NomeUnidade = param.NomeUnidade;
                indicador.Descricao = param.Descricao;

                foreach (var comparacao in indicador.Comparacoes)
                {
                    var obj = new Comparacao();

                    if (comparacao.Id > 0)
                    {
                        await repository.ComparacaoRemove(comparacao);
                    }
                }

                await repository.Change(indicador);


                indicador.Comparacoes = new List<Comparacao>();

                foreach (var comparacao in param.Comparacoes)
                {
                    await repository.AddComparacao(new Comparacao()
                    {
                        IndicadorId = indicador.Id,
                        CampoTematico = comparacao.CampoTematico,
                        NomeIndicador = comparacao.NomeIndicador,
                        Sld = comparacao.Sld
                    });
                }                

                return CustomResponse(indicador);
            }
            catch (Exception ex)
            {
                AdicionarErroProcessamento(ex.Message);
                return CustomResponse();
            }
        }

        [HttpGet("delete/{Id}")]
        [Authorize]
        public async Task<ActionResult> Delete(Int32 Id)
        {
            try
            {
                var response = await repository.Remove(Id);
                return CustomResponse(response);
            }
            catch (Exception ex)
            {
                AdicionarErroProcessamento(ex.Message);
                return CustomResponse();
            }
        }

    }
}