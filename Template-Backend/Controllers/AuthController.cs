﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using PNAD_Backend.Configuration;
using PNAD_Backend.Domain.DTO;
using PNAD_Backend.Responses;
using PNAD_Backend.Service;

namespace PNAD_Backend.Controllers
{
    [Route("api/auth")]
    [ApiController]
    public class AuthController : MainController
    {
        private readonly UserCredentialService userCredentialService;
        private readonly AppSettings appSettings;

        public AuthController(IOptions<UserCredentialService> myConfiguration, IOptions<AppSettings> _appSettings)
        {
            userCredentialService = myConfiguration.Value;
            this.appSettings = _appSettings.Value;
        }

        [HttpPost("login")]
        public async Task<ActionResult> Login(UserLoginDTO userLogin)
        {
            try
            {
                return await DoLogin(userLogin);
            }
            catch (Exception ex)
            {
                AdicionarErroProcessamento(ex.Message);
                return CustomResponse();
            }
        }

        private async Task<ActionResult> DoLogin(UserLoginDTO userLogin)
        {
            if (!ModelState.IsValid) return CustomResponse(ModelState);

            if(userLogin.Email == userCredentialService.Email && userLogin.Senha == userCredentialService.Password)
            {
                return CustomResponse(GerarJwt(userLogin.Email));
            }

            AdicionarErroProcessamento("Usuário ou Senha incorretos");
            return CustomResponse();
        }

        private UserResponseLogin GerarJwt(string email)
        {
            var identityClaims = new ClaimsIdentity();            

            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.UTF8.GetBytes(appSettings.Secret);
            var token = tokenHandler.CreateToken(new SecurityTokenDescriptor
            {
                Issuer = appSettings.Emissor,
                Audience = appSettings.ValidoEm,
                Subject = identityClaims,
                Expires = DateTime.UtcNow.AddHours(appSettings.ExpiracaoHoras),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            });

            var encodedToken = tokenHandler.WriteToken(token);

            var response = new UserResponseLogin
            {
                AccessToken = encodedToken,
                Email = email,
                ExpiresIn = TimeSpan.FromHours(appSettings.ExpiracaoHoras).TotalSeconds,                
            };

            return response;
        }

        private static long ToUnixEpochDate(DateTime date)
                => (long)Math.Round((date.ToUniversalTime() - new DateTimeOffset(1970, 1, 1, 0, 0, 0, TimeSpan.Zero)).TotalSeconds);
    }
}