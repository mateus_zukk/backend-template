﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace PNAD_Backend.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ConfigController : ControllerBase
    {

        private readonly ILogger<ConfigController> _logger;

        public ConfigController(ILogger<ConfigController> logger)
        {
            _logger = logger;
        }

        [HttpGet("groups-defined")]
        public async Task<IActionResult> GetGrupConfig()
        {
            var contents = await System.IO.File.ReadAllTextAsync("bdGrup.json", Encoding.UTF8);
            return Content(contents, "application/json");
        }
    }
}
