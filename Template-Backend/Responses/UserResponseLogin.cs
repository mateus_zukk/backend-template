﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PNAD_Backend.Responses
{
    public class UserResponseLogin
    {
        public string AccessToken { get; set; }
        public string Email { get; set; }
        public double ExpiresIn { get; set; }        
    }
}
