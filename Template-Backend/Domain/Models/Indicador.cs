﻿using PNAD_Backend.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace PNAD_Backend.Models
{
    [Table("T_INDICADORES")]
    public class Indicador : IAggregateRoot
    {
        [Column("ID_INDICADOR")]
        [Key]
        public Int32 Id { get; set; }

        [Column("ID_TEMA")]
        public Int32 TemaId{ get; set; }

        [Column("ID_RECORTE_GEOGRAFICO")]
        public Int32 RecorteGeograficoId { get; set; }

        [Column("NM_INDICADOR")]
        public string Indicadores { get; set; }

        [Column("NM_CARD")]
        public string Card { get; set; }

        [Column("NM_TEMPORALIDADE")]
        public string Temporalidade { get; set; }

        [Column("FILTRO_ESPACIAL")]
        public string FiltroEspacial { get; set; }

        [Column("NM_CAMPO_TEMATICO")]
        public string CampoTematico { get; set; }

        [Column("NM_CAMPO_TEMPORAL")]
        public string CampoTemporal { get; set; }


        //[Column("URL_POLIGONO")]
        //public string UrlPoligono { get; set; }

        [Column("SLD_POLIGONO")]
        public string Sld { get; set; }
        

        //[Column("URL_PONTO")]
        //public string UrlPonto { get; set; }

        [Column("URL_GEOSERVER")]
        public string UrlGeoServer { get; set; }

        [Column("NM_CAMADA_WFS")]
        public string CamadaWFS { get; set; }

        [Column("NM_CAMADA_WMS")]
        public string CamadaWMS { get; set; }

        [Column("TP_GRAFICO")]
        public string TipoGrafico { get; set; }

        [Column("NM_UNIDADE")]
        public string NomeUnidade { get; set; }

        [Column("NM_ICONE")]
        public string Icone { get; set; }

        [Column("VL_MINIMO")]
        public Double ValorMinimo { get; set; }

        [Column("VL_MAX")]
        public Double ValorMaximo { get; set; }

        [Column("ORDEM")]
        public Int32 Ordem { get; set; }

        [Column("NM_DESCRICAO")]
        public string Descricao { get; set; }

        public Tema Tema { get; set; }
        public RecorteGeografico Recorte { get; set; }
        public IList<Comparacao> Comparacoes { get; set; }
    }
}
