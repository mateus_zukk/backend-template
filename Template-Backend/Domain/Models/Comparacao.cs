﻿using PNAD_Backend.Interfaces;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PNAD_Backend.Models
{
    [Table("T_COMPARACAO")]
    public class Comparacao : IAggregateRoot
    {
        [Column("ID_COMPARACAO")]
        [Key]
        public Int32 Id { get; set; }

        [Column("ID_INDICADOR")]
        [ForeignKey(nameof(Indicador))]        
        public Int32 IndicadorId { get; set; }
        public Indicador Indicador;

        [Column("NM_INDICADOR")]
        public string NomeIndicador { get; set; }

        [Column("NM_CAMPO_TEMATICO")]
        public string CampoTematico { get; set; }

        [Column("NM_SLD")]
        public string Sld { get; set; }        
    }
}
