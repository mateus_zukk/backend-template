﻿using PNAD_Backend.Interfaces;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PNAD_Backend.Models
{
    [Table("T_CONFIGURACAO")]
    public class Configuracao : IAggregateRoot
    {
        [Column("ID_CONFIGURACAO")]
        [Key]
        public Int32 Id { get; set; }

        [Column("LINK_TEXT")]
        public string Link { get; set; }

        [Column("DT_ATUALIZACAO")]
        public string DataAtualizacao { get; set; }

        [Column("NM_LABEL_1")]
        public string Label1 { get; set; }

        [Column("NM_LABEL_2")]
        public string Label2 { get; set; }

        [Column("NM_LABEL_3")]
        public string Label3 { get; set; }

        [Column("NM_LABEL_4")]
        public string Label4 { get; set; }

        [Column("NM_TITULO")]
        public string Titulo { get; set; }

        [Column(TypeName = "IMG_LOGO")]
        public string Logo { get; set; }
    }
}
