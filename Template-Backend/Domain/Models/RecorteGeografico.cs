﻿using PNAD_Backend.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace PNAD_Backend.Models
{
    [Table("T_RECORTE_GEOGRAFICO")]
    public class RecorteGeografico : IAggregateRoot
    {
        [Column("ID_RECORTE_GEOGRAFICO")]
        [Key]
        public Int32 Id { get; set; }

        [Column("NM_RECORTE")]
        public string Nome { get; set; }

        [Column("ORDEM")]
        public Int16 Ordem { get; set; }
    }
}
