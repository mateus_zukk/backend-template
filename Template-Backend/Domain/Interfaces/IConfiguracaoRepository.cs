﻿using PNAD_Backend.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PNAD_Backend.Domain.Interfaces
{
    public interface IConfiguracaoRepository : IRepository<Configuracao>
    {
        Task<Configuracao> Add(Configuracao element);
        Task<Boolean> Remove(Guid guid);
        Task<Configuracao> GetFirstAllDefault();
        Task<Boolean> Change(Configuracao element);
    }
}
