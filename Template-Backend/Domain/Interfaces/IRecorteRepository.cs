﻿using PNAD_Backend.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PNAD_Backend.Domain.Interfaces
{
    public interface IRecorteRepository : IRepository<RecorteGeografico>
    {
        Task<RecorteGeografico> Add(RecorteGeografico element);
        Task<Boolean> Remove(int guid);
        Task<IList<RecorteGeografico>> GetAll();
        Task<Boolean> Change(RecorteGeografico tema);
    }
}
