﻿using PNAD_Backend.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PNAD_Backend.Domain.Interfaces
{
    public interface ITemaRepository : IRepository<Tema>
    {
        Task<Tema> Add(Tema element);
        Task<Boolean> Remove(int guid);
        Task<IList<Tema>> GetAll();
        Task<Boolean> Change(Tema tema);
    }
}
