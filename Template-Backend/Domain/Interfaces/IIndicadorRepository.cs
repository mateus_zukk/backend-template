﻿using PNAD_Backend.Domain.DTO;
using PNAD_Backend.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PNAD_Backend.Domain.Interfaces
{
    public interface IIndicadorRepository : IRepository<Indicador>
    {
        Task<Indicador> Add(Indicador element);
        Task<Boolean> Remove(Int32 guid);
        Task<Indicador> GetFirstAllDefault();
        Task<Indicador> Find(int key);
        Task<Boolean> Change(Indicador element);
        Task<Boolean> ComparacaoRemove(Comparacao comparacao);
        Task<Comparacao> AddComparacao(Comparacao comparacao);
        Task<IList<Indicador>> FindAll();

        Task<IEnumerable<IndicadorResponseDTO>> FindIndicators();
    }
}
