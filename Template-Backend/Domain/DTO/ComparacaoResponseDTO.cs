﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PNAD_Backend.Domain.DTO
{
    public class ComparacaoResponseDTO
    {        
        public Int32 Id { get; set; }        
        public Int32 IndicadorId { get; set; }                
        public string NomeIndicador { get; set; }        
        public string CampoTematico { get; set; }        
        public string Sld { get; set; }
    }
}
