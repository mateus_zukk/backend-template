﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PNAD_Backend.Domain.DTO
{
    public class IndicadorResponseDTO
    {
        public string Id { get; set; }
        public string Tema { get; set; }
        public string DesagregacaoRegional { get; set; }
        public string Indicadores { get; set; }
        public string NomeCard { get; set; }
        public string Temporalidade { get; set; }
        public string FiltroEspacial { get; set; }
        public string NomeCampoTematico { get; set; }
        public string NomeCampoTemporal { get; set; }
        public string NomeEntidadePoligonal { get; set; }
        public string UrlPolgono { get; set; }
        public string SLDPoligono { get; set; }
        public string NomeFeicaoGrafico { get; set; }
        public string UrlPonto { get; set; }
        public string UrlGeoserver { get; set; }
        public Int32 Ordem { get; set; }        
    }
}
