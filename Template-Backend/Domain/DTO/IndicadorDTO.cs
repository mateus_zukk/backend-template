﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace PNAD_Backend.Domain.DTO
{
    public class IndicadorDTO
    {
        [Required(ErrorMessage = "O campo {0} é obrigatório")]
        public Int32 TemaId { get; set; }

        [Required(ErrorMessage = "O campo {0} é obrigatório")]
        public Int32 RecorteGeograficoId { get; set; }

        [Required(ErrorMessage = "O campo {0} é obrigatório")]
        public string Card { get; set; }

        [Required(ErrorMessage = "O campo {0} é obrigatório")]
        public string Temporalidade { get; set; }

        [Required(ErrorMessage = "O campo {0} é obrigatório")]
        public string FiltroEspacial { get; set; }

        [Required(ErrorMessage = "O campo {0} é obrigatório")]
        public string CampoTematico { get; set; }

        [Required(ErrorMessage = "O campo {0} é obrigatório")]
        public string Indicadores { get; set; }

        [Required(ErrorMessage = "O campo {0} é obrigatório")]
        public string CampoTemporal { get; set; }        

        [Required(ErrorMessage = "O campo {0} é obrigatório")]
        public string Sld { get; set; }        

        [Required(ErrorMessage = "O campo {0} é obrigatório")]
        public string UrlGeoServer { get; set; }

        [Required(ErrorMessage = "O campo {0} é obrigatório")]
        public string CamadaWFS { get; set; }

        [Required(ErrorMessage = "O campo {0} é obrigatório")]
        public string CamadaWMS { get; set; }

        [Required(ErrorMessage = "O campo {0} é obrigatório")]
        public string TipoGrafico { get; set; }

        [Required(ErrorMessage = "O campo {0} é obrigatório")]
        public string NomeUnidade { get; set; }

        [Required(ErrorMessage = "O campo {0} é obrigatório")]
        public string Icone { get; set; }

        [Required(ErrorMessage = "O campo {0} é obrigatório")]
        public Double ValorMinimo { get; set; }

        [Required(ErrorMessage = "O campo {0} é obrigatório")]
        public Double ValorMaximo { get; set; }

        [Required(ErrorMessage = "O campo {0} é obrigatório")]
        public Int32 Ordem { get; set; }

        public string Descricao { get; set; }
        public IList<ComparacaoResponseDTO> Comparacoes { get; set; }
    }
}
