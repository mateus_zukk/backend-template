﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PNAD_Backend.Domain.DTO
{
    public class ConfiguracaoDTO
    {        
        public string Link { get; set; }
        
        public string DataAtualizacao { get; set; }
        
        public string Label1 { get; set; }
        
        public string Label2 { get; set; }
        
        public string Label3 { get; set; }
        
        public string Label4 { get; set; }
        
        public string Titulo { get; set; }
        
        public string Logo { get; set; }
    }
}
