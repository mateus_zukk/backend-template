﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace PNAD_Backend.Migrations
{
    public partial class CamposID : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Id",
                schema: "pnadc",
                table: "T_TEMA",
                newName: "ID_TEMA");

            migrationBuilder.RenameColumn(
                name: "Id",
                schema: "pnadc",
                table: "T_RECORTE_GEOGRAFICO",
                newName: "ID_RECORTE_GEOGRAFICO");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "ID_TEMA",
                schema: "pnadc",
                table: "T_TEMA",
                newName: "Id");

            migrationBuilder.RenameColumn(
                name: "ID_RECORTE_GEOGRAFICO",
                schema: "pnadc",
                table: "T_RECORTE_GEOGRAFICO",
                newName: "Id");
        }
    }
}
