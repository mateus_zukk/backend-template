﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace PNAD_Backend.Migrations
{
    public partial class Indicadorcampodescricao : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "VLR_MINIMO",
                schema: "pnadc",
                table: "T_INDICADORES",
                newName: "VL_MINIMO");

            migrationBuilder.RenameColumn(
                name: "VLR_MAX",
                schema: "pnadc",
                table: "T_INDICADORES",
                newName: "VL_MAX");

            migrationBuilder.AddColumn<string>(
                name: "NM_DESCRICAO",
                schema: "pnadc",
                table: "T_INDICADORES",
                type: "varchar(255)",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "NM_DESCRICAO",
                schema: "pnadc",
                table: "T_INDICADORES");

            migrationBuilder.RenameColumn(
                name: "VL_MINIMO",
                schema: "pnadc",
                table: "T_INDICADORES",
                newName: "VLR_MINIMO");

            migrationBuilder.RenameColumn(
                name: "VL_MAX",
                schema: "pnadc",
                table: "T_INDICADORES",
                newName: "VLR_MAX");
        }
    }
}
