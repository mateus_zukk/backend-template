﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace PNAD_Backend.Migrations
{
    public partial class AutalizacaoCampoRecorte : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "NM_TEMA",
                schema: "pnadc",
                table: "T_RECORTE_GEOGRAFICO",
                newName: "NM_RECORTE");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "NM_RECORTE",
                schema: "pnadc",
                table: "T_RECORTE_GEOGRAFICO",
                newName: "NM_TEMA");
        }
    }
}
