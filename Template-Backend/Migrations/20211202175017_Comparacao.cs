﻿using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace PNAD_Backend.Migrations
{
    public partial class Comparacao : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Comparacoes",
                schema: "pnadc",
                columns: table => new
                {
                    ID_COMPARACAO = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    ID_INDICADOR = table.Column<int>(nullable: false),
                    NM_INDICADOR = table.Column<string>(type: "varchar(255)", nullable: true),
                    NM_CAMPO_TEMATICO = table.Column<string>(type: "varchar(255)", nullable: true),
                    NM_SLD = table.Column<string>(type: "varchar(255)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Comparacoes", x => x.ID_COMPARACAO);
                    table.ForeignKey(
                        name: "FK_Comparacoes_T_INDICADORES_ID_INDICADOR",
                        column: x => x.ID_INDICADOR,
                        principalSchema: "pnadc",
                        principalTable: "T_INDICADORES",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Comparacoes_ID_INDICADOR",
                schema: "pnadc",
                table: "Comparacoes",
                column: "ID_INDICADOR");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Comparacoes",
                schema: "pnadc");
        }
    }
}
