﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace PNAD_Backend.Migrations
{
    public partial class ComapacaoNomenclaturas : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "CAMPO_TITULO",
                schema: "pnadc",
                table: "T_CONFIGURACAO",
                newName: "NM_TITULO");

            migrationBuilder.RenameColumn(
                name: "CAMPO_LABEL_4",
                schema: "pnadc",
                table: "T_CONFIGURACAO",
                newName: "NM_LABEL_4");

            migrationBuilder.RenameColumn(
                name: "CAMPO_LABEL_3",
                schema: "pnadc",
                table: "T_CONFIGURACAO",
                newName: "NM_LABEL_3");

            migrationBuilder.RenameColumn(
                name: "CAMPO_LABEL_2",
                schema: "pnadc",
                table: "T_CONFIGURACAO",
                newName: "NM_LABEL_2");

            migrationBuilder.RenameColumn(
                name: "CAMPO_LABEL_1",
                schema: "pnadc",
                table: "T_CONFIGURACAO",
                newName: "NM_LABEL_1");

            migrationBuilder.RenameColumn(
                name: "CAMPO_DATA_ATUALIZACAO",
                schema: "pnadc",
                table: "T_CONFIGURACAO",
                newName: "DT_ATUALIZACAO");

            migrationBuilder.RenameColumn(
                name: "Id",
                schema: "pnadc",
                table: "T_CONFIGURACAO",
                newName: "ID_CONFIGURACAO");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "NM_TITULO",
                schema: "pnadc",
                table: "T_CONFIGURACAO",
                newName: "CAMPO_TITULO");

            migrationBuilder.RenameColumn(
                name: "NM_LABEL_4",
                schema: "pnadc",
                table: "T_CONFIGURACAO",
                newName: "CAMPO_LABEL_4");

            migrationBuilder.RenameColumn(
                name: "NM_LABEL_3",
                schema: "pnadc",
                table: "T_CONFIGURACAO",
                newName: "CAMPO_LABEL_3");

            migrationBuilder.RenameColumn(
                name: "NM_LABEL_2",
                schema: "pnadc",
                table: "T_CONFIGURACAO",
                newName: "CAMPO_LABEL_2");

            migrationBuilder.RenameColumn(
                name: "NM_LABEL_1",
                schema: "pnadc",
                table: "T_CONFIGURACAO",
                newName: "CAMPO_LABEL_1");

            migrationBuilder.RenameColumn(
                name: "DT_ATUALIZACAO",
                schema: "pnadc",
                table: "T_CONFIGURACAO",
                newName: "CAMPO_DATA_ATUALIZACAO");

            migrationBuilder.RenameColumn(
                name: "ID_CONFIGURACAO",
                schema: "pnadc",
                table: "T_CONFIGURACAO",
                newName: "Id");
        }
    }
}
