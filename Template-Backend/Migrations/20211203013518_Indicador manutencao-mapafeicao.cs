﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace PNAD_Backend.Migrations
{
    public partial class Indicadormanutencaomapafeicao : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "NM_MAPA_FEICAO",
                schema: "pnadc",
                table: "T_INDICADORES");

            migrationBuilder.DropColumn(
                name: "NM_FEICAO_GRAFICO",
                schema: "pnadc",
                table: "T_INDICADORES");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "NM_MAPA_FEICAO",
                schema: "pnadc",
                table: "T_INDICADORES",
                type: "varchar(255)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "NM_FEICAO_GRAFICO",
                schema: "pnadc",
                table: "T_INDICADORES",
                type: "varchar(255)",
                nullable: true);
        }
    }
}
