﻿using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace PNAD_Backend.Migrations
{
    public partial class FirstMigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            //migrationBuilder.EnsureSchema(
            //    name: "pnadc");

            migrationBuilder.CreateTable(
                name: "T_CONFIGURACAO",
                schema: "pnadc",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    LINK_TEXT = table.Column<string>(type: "varchar(255)", nullable: true),
                    CAMPO_DATA_ATUALIZACAO = table.Column<string>(type: "varchar(255)", nullable: true),
                    CAMPO_LABEL_1 = table.Column<string>(type: "varchar(255)", nullable: true),
                    CAMPO_LABEL_2 = table.Column<string>(type: "varchar(255)", nullable: true),
                    CAMPO_LABEL_3 = table.Column<string>(type: "varchar(255)", nullable: true),
                    CAMPO_LABEL_4 = table.Column<string>(type: "varchar(255)", nullable: true),
                    CAMPO_TITULO = table.Column<string>(type: "varchar(255)", nullable: true),
                    Logo = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_T_CONFIGURACAO", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "T_RECORTE_GEOGRAFICO",
                schema: "pnadc",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    NM_TEMA = table.Column<string>(type: "varchar(255)", nullable: true),
                    ORDEM = table.Column<short>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_T_RECORTE_GEOGRAFICO", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "T_TEMA",
                schema: "pnadc",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    NM_TEMA = table.Column<string>(type: "varchar(255)", nullable: true),
                    ORDEM = table.Column<short>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_T_TEMA", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "T_INDICADORES",
                schema: "pnadc",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    ID_TEMA = table.Column<int>(nullable: false),
                    ID_RECORTE_GEOGRAFICO = table.Column<int>(nullable: false),
                    NM_INDICADOR = table.Column<string>(type: "varchar(255)", nullable: true),
                    NM_CARD = table.Column<string>(type: "varchar(255)", nullable: true),
                    NM_TEMPORALIDADE = table.Column<string>(type: "varchar(255)", nullable: true),
                    FILTRO_ESPACIAL = table.Column<string>(type: "varchar(255)", nullable: true),
                    NM_CAMPO_TEMATICO = table.Column<string>(type: "varchar(255)", nullable: true),
                    NM_CAMPO_TEMPORAL = table.Column<string>(type: "varchar(255)", nullable: true),
                    NM_MAPA_FEICAO = table.Column<string>(type: "varchar(255)", nullable: true),
                    URL_POLIGONO = table.Column<string>(type: "varchar(255)", nullable: true),
                    SLD_POLIGONO = table.Column<string>(type: "varchar(255)", nullable: true),
                    URL_PONTO = table.Column<string>(type: "varchar(255)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_T_INDICADORES", x => x.Id);
                    table.ForeignKey(
                        name: "FK_T_INDICADORES_T_RECORTE_GEOGRAFICO_ID_RECORTE_GEOGRAFICO",
                        column: x => x.ID_RECORTE_GEOGRAFICO,
                        principalSchema: "pnadc",
                        principalTable: "T_RECORTE_GEOGRAFICO",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_T_INDICADORES_T_TEMA_ID_TEMA",
                        column: x => x.ID_TEMA,
                        principalSchema: "pnadc",
                        principalTable: "T_TEMA",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_T_INDICADORES_ID_RECORTE_GEOGRAFICO",
                schema: "pnadc",
                table: "T_INDICADORES",
                column: "ID_RECORTE_GEOGRAFICO");

            migrationBuilder.CreateIndex(
                name: "IX_T_INDICADORES_ID_TEMA",
                schema: "pnadc",
                table: "T_INDICADORES",
                column: "ID_TEMA");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "T_CONFIGURACAO",
                schema: "pnadc");

            migrationBuilder.DropTable(
                name: "T_INDICADORES",
                schema: "pnadc");

            migrationBuilder.DropTable(
                name: "T_RECORTE_GEOGRAFICO",
                schema: "pnadc");

            migrationBuilder.DropTable(
                name: "T_TEMA",
                schema: "pnadc");
        }
    }
}
