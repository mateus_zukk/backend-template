﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace PNAD_Backend.Migrations
{
    public partial class IndicadorAlteracao : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "URL_POLIGONO",
                schema: "pnadc",
                table: "T_INDICADORES");

            migrationBuilder.DropColumn(
                name: "URL_PONTO",
                schema: "pnadc",
                table: "T_INDICADORES");

            migrationBuilder.RenameColumn(
                name: "WORKSPACE_GEOSERVER",
                schema: "pnadc",
                table: "T_INDICADORES",
                newName: "TP_GRAFICO");

            migrationBuilder.AddColumn<string>(
                name: "NM_CAMADA_WFS",
                schema: "pnadc",
                table: "T_INDICADORES",
                type: "varchar(255)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "NM_CAMADA_WMS",
                schema: "pnadc",
                table: "T_INDICADORES",
                type: "varchar(255)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "NM_ICONE",
                schema: "pnadc",
                table: "T_INDICADORES",
                type: "varchar(255)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "NM_UNIDADE",
                schema: "pnadc",
                table: "T_INDICADORES",
                type: "varchar(255)",
                nullable: true);

            migrationBuilder.AddColumn<double>(
                name: "VLR_MAX",
                schema: "pnadc",
                table: "T_INDICADORES",
                nullable: false,
                defaultValue: 0.0);

            migrationBuilder.AddColumn<double>(
                name: "VLR_MINIMO",
                schema: "pnadc",
                table: "T_INDICADORES",
                nullable: false,
                defaultValue: 0.0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "NM_CAMADA_WFS",
                schema: "pnadc",
                table: "T_INDICADORES");

            migrationBuilder.DropColumn(
                name: "NM_CAMADA_WMS",
                schema: "pnadc",
                table: "T_INDICADORES");

            migrationBuilder.DropColumn(
                name: "NM_ICONE",
                schema: "pnadc",
                table: "T_INDICADORES");

            migrationBuilder.DropColumn(
                name: "NM_UNIDADE",
                schema: "pnadc",
                table: "T_INDICADORES");

            migrationBuilder.DropColumn(
                name: "VLR_MAX",
                schema: "pnadc",
                table: "T_INDICADORES");

            migrationBuilder.DropColumn(
                name: "VLR_MINIMO",
                schema: "pnadc",
                table: "T_INDICADORES");

            migrationBuilder.RenameColumn(
                name: "TP_GRAFICO",
                schema: "pnadc",
                table: "T_INDICADORES",
                newName: "WORKSPACE_GEOSERVER");

            migrationBuilder.AddColumn<string>(
                name: "URL_POLIGONO",
                schema: "pnadc",
                table: "T_INDICADORES",
                type: "text",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "URL_PONTO",
                schema: "pnadc",
                table: "T_INDICADORES",
                type: "text",
                nullable: true);
        }
    }
}
