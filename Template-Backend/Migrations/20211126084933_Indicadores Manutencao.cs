﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace PNAD_Backend.Migrations
{
    public partial class IndicadoresManutencao : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "URL_GEOSERVER",
                schema: "pnadc",
                table: "T_INDICADORES",
                type: "varchar(255)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "WORKSPACE_GEOSERVER",
                schema: "pnadc",
                table: "T_INDICADORES",
                type: "varchar(255)",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "URL_GEOSERVER",
                schema: "pnadc",
                table: "T_INDICADORES");

            migrationBuilder.DropColumn(
                name: "WORKSPACE_GEOSERVER",
                schema: "pnadc",
                table: "T_INDICADORES");
        }
    }
}
