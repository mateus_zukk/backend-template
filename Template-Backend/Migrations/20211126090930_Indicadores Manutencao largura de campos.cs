﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace PNAD_Backend.Migrations
{
    public partial class IndicadoresManutencaolarguradecampos : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "URL_PONTO",
                schema: "pnadc",
                table: "T_INDICADORES",
                type: "text",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "varchar(255)",
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "URL_POLIGONO",
                schema: "pnadc",
                table: "T_INDICADORES",
                type: "text",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "varchar(255)",
                oldNullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "URL_PONTO",
                schema: "pnadc",
                table: "T_INDICADORES",
                type: "varchar(255)",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "text",
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "URL_POLIGONO",
                schema: "pnadc",
                table: "T_INDICADORES",
                type: "varchar(255)",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "text",
                oldNullable: true);
        }
    }
}
