﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace PNAD_Backend.Migrations
{
    public partial class Comparacaov2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Comparacoes_T_INDICADORES_ID_INDICADOR",
                schema: "pnadc",
                table: "Comparacoes");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Comparacoes",
                schema: "pnadc",
                table: "Comparacoes");

            migrationBuilder.RenameTable(
                name: "Comparacoes",
                schema: "pnadc",
                newName: "T_COMPARACAO",
                newSchema: "pnadc");

            migrationBuilder.RenameIndex(
                name: "IX_Comparacoes_ID_INDICADOR",
                schema: "pnadc",
                table: "T_COMPARACAO",
                newName: "IX_T_COMPARACAO_ID_INDICADOR");

            migrationBuilder.AddPrimaryKey(
                name: "PK_T_COMPARACAO",
                schema: "pnadc",
                table: "T_COMPARACAO",
                column: "ID_COMPARACAO");

            migrationBuilder.AddForeignKey(
                name: "FK_T_COMPARACAO_T_INDICADORES_ID_INDICADOR",
                schema: "pnadc",
                table: "T_COMPARACAO",
                column: "ID_INDICADOR",
                principalSchema: "pnadc",
                principalTable: "T_INDICADORES",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_T_COMPARACAO_T_INDICADORES_ID_INDICADOR",
                schema: "pnadc",
                table: "T_COMPARACAO");

            migrationBuilder.DropPrimaryKey(
                name: "PK_T_COMPARACAO",
                schema: "pnadc",
                table: "T_COMPARACAO");

            migrationBuilder.RenameTable(
                name: "T_COMPARACAO",
                schema: "pnadc",
                newName: "Comparacoes",
                newSchema: "pnadc");

            migrationBuilder.RenameIndex(
                name: "IX_T_COMPARACAO_ID_INDICADOR",
                schema: "pnadc",
                table: "Comparacoes",
                newName: "IX_Comparacoes_ID_INDICADOR");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Comparacoes",
                schema: "pnadc",
                table: "Comparacoes",
                column: "ID_COMPARACAO");

            migrationBuilder.AddForeignKey(
                name: "FK_Comparacoes_T_INDICADORES_ID_INDICADOR",
                schema: "pnadc",
                table: "Comparacoes",
                column: "ID_INDICADOR",
                principalSchema: "pnadc",
                principalTable: "T_INDICADORES",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
