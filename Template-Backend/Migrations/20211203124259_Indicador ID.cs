﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace PNAD_Backend.Migrations
{
    public partial class IndicadorID : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Id",
                schema: "pnadc",
                table: "T_INDICADORES",
                newName: "ID_INDICADOR");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "ID_INDICADOR",
                schema: "pnadc",
                table: "T_INDICADORES",
                newName: "Id");
        }
    }
}
